<?php namespace C4\FrontendBundle\Services;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class KernelListener
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
//        if ($request->query->get('option') == 2) {
//            $event->setResponse(new Response("hello, message here"));
//        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response = $event->getResponse();
        $request = $event->getRequest();

//        $response = new Response('hi', 200);
////        $response->headers->set('X-Status-Code', 200);
//        $event->setResponse($response);
//        return;

//        if ($request->query->get('option') == 3) {
//            $response->headers->setCookie(new Cookie("test", 1));
//        }

//        // only do something when the requested format is "json"
//        if ($request->getRequestFormat() != 'json') {
//            return;
//        }
//
//        // only do something when the client accepts "text/html" as response format
//        if (false === strpos($request->headers->get('Accept'), 'text/html')) {
//            return;
//        }
//
//        // set the "Content-Type" header of the response
//        $event->getResponse()->headers->set('Content-Type', 'text/plain');
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        if ($exception->getCode() == 900) {
            $message = $exception->getMessage();
            $response = new RedirectResponse($message);
            $event->setResponse($response);
        }
    }

}