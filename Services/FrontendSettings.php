<?php namespace C4\FrontendBundle\Services;

use Symfony\Component\Yaml\Yaml;

class FrontendSettings
{
    public $settings = null;

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        // load yml
        $this->settings = yaml::parse(file_get_contents(__DIR__."/../Resources/config/settings.yml"));
    }
}