<?php namespace C4\FrontendBundle\Services;

use C4\FrontendBundle\Blocks\BaseBlock;
use C4\FrontendBundle\Services\FrontendBlocks;
use C4\FrontendBundle\Models\User;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Yaml\Yaml;

class FrontendManager
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Session
     */
    public $session;

    /**
     * @var \C4\ApiBundle\Services\Api|mixed|object
     */
    public $api;

    /**
     * @var null
     */
    private $settings = null;

    /**
     * @var null
     */
    private $blocks = null;

    /**
     * @var null
     */
    private $validation = null;

    /**
     * @var null
     */
    private $user = null;

    /**
     * FrontendManager constructor.
     * @param Container $container
     */
    function __construct(Container $container)
    {
        $this->container = $container;
        $this->session = new Session();
        $this->api = $this->container->get("api");
    }

    /**
     * @param null $data
     * @param bool $reset
     * @return FrontendSettings|mixed|null
     */
    public function settings($data = null, $reset = false)
    {
        if ($reset) {
            $this->settings = new FrontendSettings();
        }

        // try to load settings from session
        if (!$this->settings) {
            // get settings from session
            $this->settings = $this->session->get("settings");
            // or initialize new settings object
            if (!$this->settings) {
                $this->settings = new FrontendSettings();
            }
        }

        // override old settings with new data
        if ($data) {
            foreach ($data as $key => $val) {
                if (property_exists($this->settings, $key)) {
                    if (is_array($this->settings->$key) && is_array($val)) {
                        $this->settings->$key = array_merge($this->settings->$key, $val);
                    } else {
                        $this->settings->$key = $val;
                    }
                }
            }
        }

        // save and return
        $this->session->set("settings", $this->settings);

        return $this->settings;
    }

    /**
     * @param null $data
     * @param bool $reset
     * @return array|mixed|null
     */
    public function blocks($data = null, $reset = false)
    {
        // todo : force reload
        $this->blocks = (new FrontendBlocks())->blocks;

//        dump( $this->blocks); die();
        return $this->blocks;

        if ($reset) {
            $this->blocks = (new FrontendBlocks())->blocks;
        }

        // try to load blocks from session
        if (!$this->blocks) {
            // get blocks from session
            $this->blocks = $this->session->get("blocks");
            // or initialize new blocks object
            if (!$this->blocks) {
                $this->blocks = (new FrontendBlocks())->blocks;
            }
        }

        // override old blocks with new data
        if ($data) {
            foreach ($data as $key => $val) {
                if (property_exists($this->blocks, $key)) {
                    if (is_array($this->blocks->$key) && is_array($val)) {
                        $this->blocks->$key = array_merge($this->blocks->$key, $val);
                    } else {
                        $this->blocks->$key = $val;
                    }
                }
            }
        }

        // save and return
        $this->session->set("blocks", $this->blocks);

        return $this->blocks;
    }

    /**
     * @return array|null
     */
    public function validation()
    {
        if (!$this->validation) {
            $validation = Yaml::parse(file_get_contents(__DIR__."/../Resources/config/validation.yml"));

            foreach ($validation as $i => $entry) {
                foreach ($entry["fields"] as $field) {
                    foreach ($entry["rules"] as $rule_key => $rule) {
                        if (!isset($this->validation[$field]) || !in_array($rule, $this->validation[$field])) {
                            $this->validation[$field][$rule_key] = $rule;
                        }
                    }
                }
            }
        }

        return $this->validation;
    }

    /**
     * @param null $data
     * @param null $group
     * @return User|mixed|null
     */
    public function user($data = null, $group = null)
    {
        if ($group) {
            $this->user = null;
            $this->session->remove("user");
        } else {
            $group = User::GROUP_GUEST;
        }

        // init user for first time
        if (!$this->user) {
            // get user from session
            $this->user = $this->session->get("user");
            // or initialize new user object
            if (!$this->user) {
                $this->user = new User();
                $this->user->group = $group;
            }
        }

        // override old user data with new data
        if ($data) {
            foreach ($data as $key => $val) {
                if (property_exists($this->user, $key)) {
                    if (is_array($this->user->$key) && is_array($val)) {
                        $this->user->$key = array_merge($this->user->$key, $val);
                    } else {
                        $this->user->$key = $val;
                    }
                }
            }
        }

        // save and return
        $this->session->set("user", $this->user);

        return $this->user;
    }

    /**
     * Token, user session, etc...
     */
    public function validateSession()
    {
        if ($this->api->token) {
            if (!$this->api->token_expires_at) {
                $this->api->token = null;
                $this->api->token_expires_at = null;
            } else {
                if (new \DateTime() > $this->api->token_expires_at) {
                    $this->api->token = null;
                    $this->api->token_expires_at = null;
                }
            }
        }
        if (!$this->api->token) {
            // if we have no token, we are guest (we cant be authorized if we have no token)
            if ($this->user()->group != User::GROUP_GUEST) {
                $this->user([], User::GROUP_GUEST);
            }
        }
    }

    /**
     * @return bool
     */
    public function ceckApi(){
        $response = $this->api->mb_checkService();
        if ($response["status"] == 200 && $response["body"]["status"] == "ok") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function checkSession(){
        $response = $this->api->mb_checkSession();
        if ($response["status"] == 200) {
            if ($response["body"]["is_active"] == true){
                //  "is_active": true,
                //  "expiration_at": "2017-08-31T12:34:59"
                $this->api->token_expires_at(new \DateTime($response["body"]["expiration_at"]));
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $email
     * @param $password
     */
    public function login($email, $password)
    {
        $response = $this->api->mb_login(["username" => $email, "password" => $password]);
        if ($response["status"] == 200) {

            $this->api->token($response["body"]["session_id"]);
            $this->api->token_expires_at(new \DateTime($response["body"]["session_expiration_at"]));

            $this->user($response["body"], User::GROUP_CLIENT);
        }

        return $response;
    }

    /**
     * @param $email
     * @param $password
     */
    public function adminLogin($username, $password)
    {
        $response = $this->api->mb_adminLogin(["username" => $username, "password" => $password]);
        if ($response["status"] == 200) {

            $this->api->token($response["body"]["session_id"]);
            $this->api->token_expires_at(new \DateTime($response["body"]["session_expiration_at"]));

            $this->user($response["body"], User::GROUP_ADMIN);
        }

        return $response;
    }

    /**
     * Clear session
     */
    public function logout()
    {
        $this->session->clear();
    }

    /**
     * @param $invoice_id
     * @return Response
     */
    public function getInvoice($invoice_id)
    {
        $response = $this->api->mb_getInvoice(["invoice_id" => $invoice_id]);
        if ($response["status"] == 200) {
            $r = new Response('Content', $response["status"]);
            $r->headers->set('Content-Type', 'application/pdf');
            $r->setContent($response['body']);

            return $r;
        }
    }

    /**
     * @param $email
     * @return array
     */
    public function passwordReset($email)
    {
        $router = $this->container->get("router");
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $url = $router->generate("frontend.get_password_reset", ["token" => "__token__"], 0);

        $response = $this->api->mb_passwordReset(
            [
                "email"            => $email,
                "confirmation_url" => $url,
            ]
        );

        return $response;
    }

    public function register($data = null)
    {
        $router = $this->container->get("router");
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $data["lang"] = $request->getLocale();

        $data["auth_provider"] = "";
        $data["auth_action_id"] = "";
        $data["examples"] = "";

        $data["confirmation_url"] = $router->generate("frontend.get_register_confirm", ["token" => "__token__"], 0);

        $response = $this->api->mb_signUp($data);

        return $response;
    }

    /**
     * @param $token
     * @return array
     */
    public function registerConfirm($token)
    {
        $response = $this->api->mb_signUpConfirm(
            [
                "token" => $token,
            ]
        );

        return $response;
    }

    /**
     * @param $token
     * @param $password
     * @return array
     */
    public function passwordResetConfirm($token, $password)
    {
        $response = $this->api->mb_passwordResetConfirm(
            [
                "token"    => $token,
                "password" => $password,
            ]
        );

        return $response;
    }

}
