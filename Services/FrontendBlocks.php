<?php namespace C4\FrontendBundle\Services;

use Symfony\Component\Yaml\Yaml;

class FrontendBlocks
{
    public $blocks = [];

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $this->blocks = yaml::parse(file_get_contents(__DIR__."/../Resources/config/blocks.yml"));
    }
}