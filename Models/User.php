<?php namespace C4\FrontendBundle\Models;

class User
{
    const GROUP_GUEST = "guest";
    const GROUP_CLIENT = "client";
    const GROUP_ADMIN = "admin";
    const GROUP_MANAGER = "manager";

    public $group = "guest";

    public $username = null;

    public $id = null;
    public $email = null;
    public $password = null;
    public $first_name = null;
    public $last_name = null;
    public $phone = null;

    public $display_name = null;
}
