<?php namespace C4\FrontendBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

class GlobalsExtension extends \Twig_Extension
{
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function getGlobals() {
        return array(
            'fm' => $this->container->get('frontend_manager'),
        );
    }

    public function getName() {
        return 'FrontendBundle:GlobalsExtension';
    }
}