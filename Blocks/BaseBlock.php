<?php namespace C4\FrontendBundle\Blocks;

use Symfony\Component\DependencyInjection\ContainerInterface;

class BaseBlock
{
    private $container;
    private $parent_block;

    public $block_name = "base_block";

    public $settings = [
        "render"     => true, // return nothing from function render()
        "standalone" => false, // if true, ignore in foreach blocks

        "tag"         => "div",
        "for"         => "",
        "id"          => "",
        "name"        => "",
        "value"       => "",
        "placeholder" => "",
        "title"       => "",
        "text"        => "",

        "class"      => [],
        "attr"       => [],
        "validation" => [],
        "blocks"     => [],

        "active"   => false,
        "disabled" => false,
        "hidden"   => false,

        "route"            => "",
        "route_parameters" => [],
        "href"             => "",
    ];

    public $standalone = false;
    public $template = "";

    public function __construct(ContainerInterface $container = null, $block_name = null, $settings = [], $parent_block = null)
    {
        if ($container == null) {
            $this->settings["render"] = false;

            return;
        }
        $this->container = $container;
        $this->block_name = $block_name;
        $this->parent_block = $parent_block;
        $this->initSettings($settings);
        $this->block_name = $this->settings["block_name"] = $block_name;
    }

    public function initSettings($settings = [])
    {
        $frontend = $this->container->get("frontend_manager");

        // load settings from yaml
        $this->settings = array_replace(
            $this->settings,
            $frontend->blocks()[$this->block_name]
        );

        // if block is ("form_group_input" | "checkbox" | "radio") and has name - parse validation for block
        if (in_array($this->block_name, ["form_group_input", "checkbox", "radio"]) && isset($settings["name"])) {
            // load validations from yaml
            if (isset($frontend->validation()["all"])) {
                $this->settings["validation"] = $frontend->validation()["all"];
            } else {
                $this->settings["validation"] = [];
            }

            if (isset($frontend->validation()[$settings["name"]])) {
                foreach ($frontend->validation()[$settings["name"]] as $i => $value) {
                    if (!in_array($value, $this->settings["validation"])) {
                        $this->settings["validation"][] = $value;
                    }
                }
            }
        }

        // override settings passed in array
        // clear empty values - we dont want to replace defaults with empty values
        foreach ($settings as $key => $val) {
            if ($val == "" && key_exists($key, $this->settings)) {
                unset($settings[$key]);
            }
        }
        // override defaults with php specified settings and values
        $this->settings = array_replace($this->settings, $settings);

        // set custom template
        if (isset($this->settings["template"])) {
            $this->template = $this->settings["template"];
        }
        if (isset($this->settings["standalone"])) {
            $this->standalone = $this->settings["standalone"];
        }
    }

    public function addBlock($block_name, $settings = [])
    {

        if (!is_string($block_name)) {
            $block = $block_name;
        } else {
            $block_class = 'C4\FrontendBundle\Blocks\\'.$this->container->get("frontend_manager")->blocks()[$block_name]["block"];
            $block = new $block_class($this->container, $block_name, $settings);
            $block->block_name = $block->settings["block_name"] = $block_name;
        }

        if ($block->settings["id"] != "") {
            $this->settings["blocks"][$block->settings["id"]] = $block;
        } else {
            $this->settings["blocks"][] = $block;
        }

        return $this;
    }

    public function &withBlock($block_name, $settings = [])
    {

        // todo - adding already created block we need to setup parent block, so endWith works...
//        if (!is_string($block_name)) {
//            $block = $block_name;
//        } else {
        $block_class = 'C4\FrontendBundle\Blocks\\'.$this->container->get("frontend_manager")->blocks()[$block_name]["block"];
        $block = new $block_class($this->container, $block_name, $settings, $this);
        $block->block_name = $block->settings["block_name"] = $block_name;
//        }

        if ($block->settings["id"] != "") {
            $this->settings["blocks"][$block->settings["id"]] = $block;
        } else {
            $this->settings["blocks"][] = $block;
        }

        return $block;
    }

    public function endWith()
    {
        return $this->parent_block;
    }

    public function render($template = null)
    {
        if (!$this->settings["render"]) {
            return "";
        }

        if ($template) {
            $this->template = $template;
        }

//        $app->profiler("blocks", $this->settings);
//        if (substr($this->block_name, 0, 6) == "input_") {
//            $app->profiler("inputs", $this->settings);
//        }

        return $this->container->get('twig')->render(
            $this->template,
            array_merge(
                [
                    "this" => $this,
                ],
                $this->settings
            )
        );
    }

    public function findBlock($id)
    {
        if (!isset($this->settings["blocks"][$id])) {
            return new self(
                $this->container, "block", [
                    "id"     => $id,
                    "render" => false,
                ]
            );
        }

        return $this->settings["blocks"][$id];
    }

    // alias
    public function getBlock($id){
        return $this->findBlock($id);
    }
    // alias
    public function block($id){
        return $this->findBlock($id);
    }

    public function get($key){
        return (isset($this->settings[$key]))?$this->settings[$key]:"";
    }

}