<?php

namespace C4\FrontendBundle\Controller;

use C4\FrontendBundle\Controller\FrontendController\ajaxCheckUsernameAction;
use C4\FrontendBundle\Controller\FrontendController\buildAdminMenu;
use C4\FrontendBundle\Controller\FrontendController\buildUserMenu;
use C4\FrontendBundle\Controller\FrontendController\getAccountBillingAction;
use C4\FrontendBundle\Controller\FrontendController\getAccountElektricityAction;
use C4\FrontendBundle\Controller\FrontendController\getAccountDashboardAction;
use C4\FrontendBundle\Controller\FrontendController\getAccountServicesAction;
use C4\FrontendBundle\Controller\FrontendController\getAdminDashboardAction;
use C4\FrontendBundle\Controller\FrontendController\getAdminLoginAction;
use C4\FrontendBundle\Controller\FrontendController\getLogoutAction;
use C4\FrontendBundle\Controller\FrontendController\getRegisterConfirmAction;
use C4\FrontendBundle\Controller\FrontendController\postAdminLoginAction;
use C4\FrontendBundle\Controller\FrontendController\postLoginAction;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

use C4\FrontendBundle\Controller\FrontendController\buildHeader;
use C4\FrontendBundle\Controller\FrontendController\buildFooter;
use C4\FrontendBundle\Controller\FrontendController\buildLoginTools;
use C4\FrontendBundle\Controller\FrontendController\buildBankList;

use C4\FrontendBundle\Controller\FrontendController\indexAction;
use C4\FrontendBundle\Controller\FrontendController\getStatusAction;
use C4\FrontendBundle\Controller\FrontendController\getRegisterAction;
use C4\FrontendBundle\Controller\FrontendController\getRegisterWithBankAction;
use C4\FrontendBundle\Controller\FrontendController\getForgotPasswordAction;
use C4\FrontendBundle\Controller\FrontendController\getPasswordResetAction;

use C4\FrontendBundle\Controller\FrontendController\postRegisterAction;
use C4\FrontendBundle\Controller\FrontendController\postForgotPasswordAction;
use C4\FrontendBundle\Controller\FrontendController\postPasswordResetAction;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class FrontendController
 * @package C4\FrontendBundle\Controller
 */
class FrontendController extends Controller
{
    public $container;
    /**
     * @var \C4\FrontendBundle\Services\FrontendManager
     */
    public $fm;
    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    public $session;
    /**
     * @var \C4\TranslationBundle\Components\Translator
     */
    public $translator;
    public $request;

    public $router;
    public $route;
    public $route_params;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        // translator
        $this->translator = $container->get('translator');

        // frontend manager
        $this->fm = $container->get("frontend_manager");

        // current request
        $this->request = $this->container->get('request_stack')->getCurrentRequest();

        // current route
        $this->router = $this->container->get('router');
        $this->route = $this->request->get('_route');
        $this->route_params = $this->request->get('_route_params');

        // session
        $this->session = new Session();

        // check if api working
        if (!$this->fm->ceckApi()){
            // todo : redirect to API DOWN
            throw new  \Exception($this->generateUrl("frontend.index"), 900);
        }

        // local token checking
        $this->fm->validateSession();

        // remote token checking
        if ($this->fm->api->token()) {
            if (!$this->fm->checkSession()){
                $this->session->clear();
                throw new  \Exception($this->generateUrl("frontend.index"), 900);
            }
        }

        // route access checking
        if (isset($this->route_params["access"]) && !in_array($this->fm->user()->group, $this->route_params["access"])) {
            throw new  \Exception($this->generateUrl("frontend.index"), 900);
        }

        // save current route in session
        $this->session->set("last_route", $this->route);
        $this->session->set("last_route_params", $this->route_params);

    }

    /*
     * <script src="/validation?t={{ date().timestamp }}"></script>
     */
    public function validationAction()
    {
        $keys = [];

        foreach ($this->fm->validation() as $field_rules) {
            foreach ($field_rules as $rule_key => $rule) {
                if ($rule_key != "do_not_translate") {
                    if (is_string($rule_key)) {
                        $key = $rule_key;
                    } else {
                        $key = str_replace("data-parsley-", "", strtok($rule, "="));
                    }

                    $keys[str_replace(["-", "_"], "", lcfirst(ucwords($key, "-_")))] = str_replace("-", "_", $key);
                }
            }
        }

        return $this->render('@Frontend/validation.js.twig', ["keys" => $keys]);
    }

    use indexAction;
}
