<?php

namespace C4\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DemoController extends Controller
{

    public function indexAction($twig, $param)
    {
        return $this->render(
            "@Frontend/demo/$twig.html.twig",
            [
                'param' => $param
            ]
        );
    }

}
