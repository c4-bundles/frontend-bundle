<?php

namespace C4\FrontendBundle\Controller\FrontendController;

use C4\FrontendBundle\Blocks\BaseBlock;

trait indexAction
{
    function indexAction()
    {
        /** @var \C4\FrontendBundle\Controller\FrontendController $this */

        $page = new BaseBlock($this->container, "block");

        return $this->render('@Frontend/pages/standard.html.twig', $page->settings);
    }
}
